package dancerSlides;
use Dancer2;
use Dancer2::Plugin::Database;

our $VERSION = '0.1';



get '/slide' => sub {
	content_type 'application/json';
	header 'access_control_allow_origin' => '*';

	my @slides = database->quick_select('slides',{});
	
	return to_json{slides =>\@slides};
};

   
post '/slide' => sub {
	content_type 'application/json';
	header 'access_control_allow_origin' => '*';

	my $position = params->{'position'};
	my $content = params->{'content'};
	my $presentation_name = params->{'presentation'};

	my $presentation = database->quick_lookup('presentation',{name => $presentation_name},'id');
 	
 	database->quick_insert('slides',{position=>$position,content=>$content,presentation_id=>$presentation});

	my $id = database->quick_lookup('slides',{presentation_id => $presentation,position => $position},'id');

	return to_json{slide =>{id => $id,position => $position,content => $content,presentation => $presentation}};
};

put '/slide' => sub {
	content_type 'application/json';
	header 'access_control_allow_origin' => 'http://localhost:9000';

	my $id = params->{'id'};
	my $position = params->{'position'};
	my $presentation = params->{'presentation'};
	my $content = params->{'content'};

	 database->quick_update('slides',{id=>$id},{content => $content});

	return  to_json{slide =>{id => $id,position => $position,presentation =>$presentation,content => $content}};
};


post '/presentation' => sub  {
	content_type 'application/json';
	header 'access_control_allow_origin' => '*';

	my $name = params->{'name'};

	database->quick_insert('presentation',{name => $name});

	return to_json{name => $name};
};

  

get '/presentation/:name' => sub {
	header 'access_control_allow_origin' => '*';

	my $presentation = database->quick_lookup('presentation',{name => param('name')},'id');

	my @slides = database->quick_select('slides',{presentation_id => $presentation});
	
	return to_json{slides =>\@slides};

};

get 'play/:name' => sub  {
	
	template 'presentation';
};



true;
