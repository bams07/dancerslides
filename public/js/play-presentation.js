(function(document, window) {

    $('#impress');

    var name = window.location.href.split('/').pop();
    var url = (window.location.origin + '/presentation/:name').replace(':name',name);

    $.get(url, function(data) {

        addSlide(data);

    }).fail(function(data) {
        console.log(data);
    });

    function addSlide(data) {

        var dataX = -1000;

        JSON.parse(data).slides.map(function(value) {

            console.log(value);

            var slide = '<div class="step slide" data-x="' + dataX + '" data-y="-1500">' +
                '' + value.content + '</div>';

            $('#impress').append(slide);

            dataX += 1000;
        });

        impress().init();

    };

})(document, window);
