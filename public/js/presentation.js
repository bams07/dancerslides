$(document).ready(function() {

    var slides = $('#slides');
    var save_slide = $('#save-slide');
    var play_presentation = $('#play-presentation');
    var save_presentation = $('#save-presentation');
    var position = 0;
    var presentation = '';
    var presentation_name = $('#presentation-name');

    save_presentation.click(function() {

        url = window.location.origin + '/presentation';
        data = {
            name: presentation_name.val()
        };
        $.post(url, data, function(data) {
            presentation = data;
            setValues();
        }).error(function(data) {
            console.log(data)
        });
    });

    save_slide.click(function() {

        position++;

        content = CKEDITOR.instances.editor.getData();
        url = window.location.origin + '/slide';
        data = {
            presentation_name: presentation_name.val()
            position: position,
            content: content
        };
        $.post(url, data)
            .success(function(data) {
                console.log(data);
                addSlide();
            }).error(function(data) {
                console.log(data);

            });

    });


    function addSlide() {

        slide = '<a id="' + position + '" href="#slide' + position + '" class="list-group-item">' +
            '<h4 class="list-group-item-heading">Slide ' + position + '</h4>' +
            '<p class="list-group-item-text">...</p></a>';

        $('#slides').append(slide);

    }

    function setValues() {

        save_slide.prop('disabled', false);
        play_presentation.prop('disabled', false);
        presentation_name.prop('disabled', true);
        save_presentation.prop('disabled', true);
        play_presentation.attr('href','/presentation/play');

    };


});
